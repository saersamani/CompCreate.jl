using CSV
using DataFrames
using LsqFit
using Dierckx
using Statistics
using BenchmarkTools

using SpecialFunctions
#using Plots




################################################################
# This module is to perform self-adjusting centroiding of HRMS profile data.
#
#
################################################################


####################################################################
#Simple moving average

function sma(y, n)

    vals = zeros(size(y,1) ,1)
    for i in 1:size(vals,1)-(n-1)
        vals[i+1] = mean(y[i:i+(n-1)])
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end

################################################################
# function to find the cent mass

function find_cent(fit,ms_vec,int_vec,m,scan)


    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))
    @.model1(x, p) =  (p[1]/pi)*(p[3]/((x-p[2])^2 + p[3]^2)) * (1+erf((1/(2*p[3]))*(x-p[2])))

    if m == 1
        y_r=model(ms_vec,fit.param)
    elseif m == 2
        y_r=model1(ms_vec,fit.param)
    end

    ms_cn = mean([ms_vec[argmax(y_r)],ms_vec[argmax(int_vec)]])
    int_cn = maximum(int_vec)

    spl = Spline1D(ms_vec,int_vec)


    mz_vec = ms_vec[1]:0.0001:ms_vec[end];
    mm = spl(mz_vec);

    ind_apx = argmax(mm);
    if isnothing(findfirst(x -> x<= int_cn/2, mm[ind_apx:end])) == 0 && findfirst(x -> x<= int_cn/2, mm[ind_apx:end]) + ind_apx < length(mz_vec)
        mz_h = mz_vec[ind_apx + findfirst(x -> x<= int_cn/2, mm[ind_apx:end])]
    else
        mz_h = mz_vec[end]
    end

    if isnothing(findlast(x -> x<= int_cn/2, mm[1:ind_apx])) == 0
        mz_l = mz_vec[findlast(x -> x<= int_cn/2, mm[1:ind_apx])]
    else
        mz_l = mz_vec[1]

    end

    dm = round(mz_h - mz_l,digits=4)
    #p = plot()
    #plot!(ms_vec,int_vec,label="raw data")
    #plot!(ms_vec,sma(int_vec,3),label="smooth data")
    #plot!(ms_vec,y_r,label="modeled data")
    #plot!([ms_cn,ms_cn],[0,int_cn],label="centroided data")
    #xlabel!("Mass (Da)")
    #ylabel!("Intensity")
    # plot(mz_vec,mm)
    # size(mz_vec)

    #pathout = "/Volumes/SAER HD/Data/Temp_files/Figs_cent/" * string(scan)* "_" * string(ms_cn) * "_" * string(int_cn) * ".png"
    #savefig(pathout)



    return(ms_cn,int_cn,dm)


end

################################################################
# function to assess the fit

function fit_assess!(fit,ms_vec,int_vec,m,dm,r2,r_thresh)

    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))
    @.model1(x, p) =  (p[1]/pi)*(p[3]/((x-p[2])^2 + p[3]^2)) * (1+erf((1/(2*p[3]))*(x-p[2])))

    if m == 1
        y_r=model(ms_vec,fit.param)
    elseif m == 2
        y_r=model1(ms_vec,fit.param)
    elseif m == 0
        return r2
    end

    if ms_vec[argmax(y_r)] - ms_vec[argmax(int_vec)] > dm/2
        r2 = 0

    end


    if argmax(y_r) ==1 || argmax(int_vec) == 1
        r2 = 0

    elseif argmax(y_r) ==length(y_r) || argmax(int_vec) == length(y_r)

        r2 = 0
    end


    return(r2)

end


################################################################
# function to fit a gaussian

function fit_gs(ms_vec,int_vec,dm,r_thresh)

    @.model1(x, p) =  (p[1]/pi)*(p[3]/((x-p[2])^2 + p[3]^2)) * (1+erf((1/(2*p[3]))*(x-p[2])))


    @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))

    ms_vec_s = ms_vec[int_vec .>= 0.3*maximum(int_vec)];
    int_vec_s = int_vec[int_vec .>= 0.3*maximum(int_vec)];
    m = 0
    # plot(ms_vec_s,int_vec_s)

    if length(ms_vec) >= 3 && length(ms_vec_s) >= 3
        p=zeros(3)
        p[1]=maximum(int_vec_s)
        p[2]=ms_vec_s[argmax(int_vec_s)]
        dm_m=(maximum(ms_vec_s)-minimum(ms_vec_s))
        p[3]=dm/2
        lbp=[0,ms_vec_s[argmax(int_vec_s)]-dm,0]
        ubp=[Inf,ms_vec_s[argmax(int_vec_s)]+dm,Inf]
        fit = []
        r2 = 0


        try
            fit = curve_fit(model ,ms_vec_s, sma(int_vec_s,3)[:], p)
            r2=1 - var(fit.resid) / var(int_vec_s)
            #println(r2)
            m = 1

        catch
            fit = []
            r2 = 0
            #println(r2)

        end
        #
        if r2 < r_thresh
            try
                fit = curve_fit(model ,ms_vec_s, sma(int_vec_s,3)[:], p,lower=lbp, upper=ubp)
                r2=1 - var(fit.resid) / var(int_vec_s)
                m = 1

            catch
                fit = []
                r2 = 0

            end

        end

        #
        if r2 < r_thresh
            try
                fit = curve_fit(model1 ,ms_vec_s, sma(int_vec_s,3)[:], p)
                r2 = 1 - var(fit.resid) / var(int_vec_s)
                m = 2
            catch
                fit = []
                r2 = 0

            end

        end


    elseif length(ms_vec) >= 3 && length(ms_vec_s) < 3
        p=zeros(3)
        p[1]=maximum(int_vec)
        p[2]=ms_vec[argmax(int_vec)]
        dm_m=(maximum(ms_vec)-minimum(ms_vec))
        p[3]=dm/2
        lbp=[0,ms_vec[argmax(int_vec)]-dm,0]
        ubp=[Inf,ms_vec[argmax(int_vec)]+dm,Inf]

        fit = []
        r2 = 0

        try
            fit = curve_fit(model ,ms_vec, sma(int_vec,3)[:], p)
            r2=1 - var(fit.resid) / var(int_vec)
            m = 1
        catch
            fit = []
            r2 = 0

        end
        #
        if r2 < r_thresh
            try
                fit = curve_fit(model ,ms_vec, sma(int_vec,3)[:], p,lower=lbp, upper=ubp)
                r2=1 - var(fit.resid) / var(int_vec)
                m = 1

                #println(r2)
            catch
                fit=[]
                r2=0

            end

        end

        #
        if r2 < r_thresh
            try
                fit = curve_fit(model1 ,ms_vec, sma(int_vec,3)[:], p)
                r2=1 - var(fit.resid) / var(int_vec)
                #println(r2)
                m = 2
            catch
                fit=[]
                r2=0

            end

        end


    else
        fit = []
        r2 = 0
        p = zeros(3)
        dm_m = 0

    end

    return(fit,r2,dm_m,m)

end






################################################################
# function to centroid

function cen_d!(mz_t,int_t,dm,ind_mx,r_thresh,s2n,scan,min_int)
    ms = mz_t[ind_mx]
    ms_vec = mz_t[findall(x -> ms + dm >= x >=ms -  dm ,mz_t)];
    int_vec = int_t[findall(x -> ms + dm >= x >=ms -  dm ,mz_t)];

    if maximum(int_vec)/median(int_vec) >= s2n

        fit,r2,dm_p,m = fit_gs(ms_vec,int_vec,dm,r_thresh);
        r2 = fit_assess!(fit,ms_vec,int_vec,m,dm,r2,r_thresh)
        if r2 >= r_thresh && length(ms_vec) > 3

            ms_cn,int_cn,dm_m = find_cent(fit,ms_vec,int_vec,m,scan)

        else

            #p1 = plot()
            #plot!(ms_vec,int_vec,label="raw data")
            #plot!(ms_vec,sma(int_vec,3),label="smooth data")
            #xlabel!("Mass (Da)")
            #ylabel!("Intensity")
            #pathout = "/Volumes/SAER HD/Data/Temp_files/Figs_cent/bad/bad_" * string(scan)* "_"* string(ms) * "_" * string(int_t[ind_mx]) * ".png"
            #savefig(pathout)
            ms_cn = []
            int_cn = []
            dm_m = 0

        end

    else
        #p1 = plot()
        #plot!(ms_vec,int_vec,label="raw data")
        #xlabel!("Mass (Da)")
        #ylabel!("Intensity")
        #pathout = "/Volumes/SAER HD/Data/Temp_files/Figs_cent/bad/bad_" * string(scan)* "_" *string(ms) * "_" * string(int_t[ind_mx]) * ".png"
        #savefig(pathout)
        ms_cn = []
        int_cn = []
        dm_m = 0
    end

    # plot(ms_vec,int_vec)

    int_t[findall(x -> ms + dm >= x >=ms -  dm ,mz_t)] .= min_int/2;

    return(ms_cn,int_cn,int_t,dm_m)


end




################################################################
# peak finder

# mz_t = mz_val[j,:]
# int_t = mz_int[j,:]

function peak_finder(mz_t,int_t,res,min_p_w,min_int,r_thresh,s2n,scan)

    mm = length(int_t[int_t .>= min_int])
    ms_cn_vec1 = zeros(mm)
    int_cn_vec1 = zeros(mm)
    dm_m_vec1 = zeros(mm);

    # i =1

    for i=1:mm
        #println(i)
        if maximum(int_t) >= min_int
            ind_mx = argmax(int_t)
            dm = mz_t[ind_mx]/res

            if dm < min_p_w
                dm =  min_p_w
            end
            ms_cn_vec,int_cn_vec,int_t,dm_m_vec = cen_d!(mz_t,int_t,dm,ind_mx,r_thresh,s2n,scan,min_int)
            if length(ms_cn_vec[ms_cn_vec .>0]) > 0
                ms_cn_vec1[i] = ms_cn_vec[ms_cn_vec .>0]
                int_cn_vec1[i] = int_cn_vec[ms_cn_vec .>0]
                dm_m_vec1[i] = dm_m_vec[dm_m_vec .>0]


            end


        else
            break

        end

    end


    return(ms_cn_vec1[ms_cn_vec1 .> 0],int_cn_vec1[int_cn_vec1 .> 0],dm_m_vec1[dm_m_vec1 .> 0])


end
