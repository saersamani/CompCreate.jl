
using Pkg
using CompCreate
using Test
using JLD2
using CSV
using DataFrames
using FileIO

##############################

try
    using MS_Import
catch
    @warn("MS_Import is being installed")
    Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/ms_import.jl/src/master/"))
     using MS_Import
end





@testset "Checking Depenedencies" begin
    #using Base


    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    chrom=import_files(path,filenames,mz_thresh)
    mm=pathof(CompCreate)
    path2aux=joinpath(mm[1:end-13],"AUX_data.jld2")

    d1=load(path2aux,"AUX_data")
    #println(pwd())
    feature_list = DataFrame(CSV.File("test_chrom_report.csv"))



    @test chrom["MS1"]["Rt"][1] == 0.022
    @test size(d1["Iso"]["ru_m"],1) == 6
    @test feature_list[!,"Nr"][1] == 1

end


@testset "comp_ms1" begin


    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    chrom=import_files(path,filenames,mz_thresh,150)
   

    delta_mass=0.004
    min_int=300
    mass_win_per=0.8
    ret_win_per=0.5
    r_thresh=0.8

    path2features="test_chrom_report.csv"
    
    components = comp_ms1(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass)
    

    @test components[!,"Parent"][1] == 0
    @test components[!,"MS1Comp"][2] == "[374.2382]"



end




@testset "compcreate" begin


    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    chrom=import_files(path,filenames,mz_thresh,150)


    delta_mass=0.004
    min_int=300
    mass_win_per=0.8
    ret_win_per=0.5
    r_thresh=0.8

    path2features="test_chrom_report.csv"

    comp_list=compcreate(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)

    @test comp_list[!,"Parent"][1] == 1
    @test comp_list[!,"Parent"][2] == 1
    @test isnan(comp_list[!,"AccuMass"][1]) ==0



end



